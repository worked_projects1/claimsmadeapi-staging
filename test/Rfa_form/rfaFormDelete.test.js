const request = require('supertest');
const { config } = require('../global');
const server = request(config.server_url);

describe('RFA Forms Delete Validation', () => {

    //Physician login
    test('Physicain user login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.testEmail,
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    test('Verify if practice admin can create a case', async () => {
        const response = await server.delete(`/rfaForms/${config.rfa_case_ids[0]}`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({

            });
        expect(response.statusCode).toBe(200);
    });

    //Staff login
    test('Staff user login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.testEmail2,
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    test('Verify if practice admin can create a case', async () => {
        const response = await server.delete(`/rfaForms/${config.rfa_case_ids[1]}`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({

            });
        expect(response.statusCode).toBe(200);
    });

});