const request = require('supertest');
const { config } = require('../global');
const server = request(config.server_url);

describe('RFA Form Update Validation', () => {
    //Physician login
    test('Physicain user login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.testEmail,
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    test('Verify if practice admin (phsician) can update a case', async () => {
        const response = await server.put(`/rfaForms/${config.rfa_case_ids[0]}`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({
                id: config.rfa_case_ids[0],
                name: "case_01_updated",
                date_of_injury: "05/01/99",
                date_of_birth: "05/02/87",
                claim_number: "987589654",
                employer: "Test User"
            });
        expect(response.statusCode).toBe(200);

        expect(config.rfa_case_ids[0]).toBe(JSON.parse(response.text).case_data.id);
        expect("case_01_updated").toBe(JSON.parse(response.text).case_data.name);
        expect('1999-04-30T18:30:00.000Z').toBe(JSON.parse(response.text).case_data.date_of_injury);
        expect("1987-05-01T18:30:00.000Z").toBe(JSON.parse(response.text).case_data.date_of_birth);
        expect("987589654").toBe(JSON.parse(response.text).case_data.claim_number);
        expect("Test User").toBe(JSON.parse(response.text).case_data.employer);
    });

    //staff login
    test('Staff user login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.testEmail2,
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    test('Verify if practice admin can update a case', async () => {
        const response = await server.put(`/rfaForms/${config.rfa_case_ids[1]}`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({
                id: config.rfa_case_ids[1],
                name: "case_02_updated",
                date_of_injury: "05/01/99",
                date_of_birth: "05/02/87",
                claim_number: "987589654",
                employer: "Test User 01"
            });
        expect(response.statusCode).toBe(200);

        expect(config.rfa_case_ids[1]).toBe(JSON.parse(response.text).case_data.id);
        expect("case_02_updated").toBe(JSON.parse(response.text).case_data.name);
        expect('1999-04-30T18:30:00.000Z').toBe(JSON.parse(response.text).case_data.date_of_injury);
        expect("1987-05-01T18:30:00.000Z").toBe(JSON.parse(response.text).case_data.date_of_birth);
        expect("987589654").toBe(JSON.parse(response.text).case_data.claim_number);
        expect("Test User 01").toBe(JSON.parse(response.text).case_data.employer);
    });

    test('Verify if practice admin can update a case', async () => {
        const response = await server.put(`/rfaForms/${config.rfa_case_ids[1]}`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({
                // name: "case_02_updated",
                date_of_injury: "05/01/99",
                date_of_birth: "05/02/87",
                claim_number: "987589654",
                employer: "Test User 01"
            });
        expect(response.statusCode).toBe(400);
        expect('The name field is required.').toBe(JSON.parse(response.text).error.name[0])
 
    });
});