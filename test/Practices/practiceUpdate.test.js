const request = require('supertest');
const { config } = require('../global');
const server = request(config.server_url);

describe('Practice update validation', () => {
    //super admin login
    test('Super admin login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[0],
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    test('Verify if super admin able to update practice', async () => {
        const response = await server.put(`/practices/${config.practice_id[2]}`)
            .set(config.headers)
            .set('Authorization', process.env.authToken)
            .send({
                name: "Super admin able to update practice",
                address: "updated Address",
                street: "updated West",
                city: "updated Madurai",
                state: "updated TamilNadu",
                zip_code: "627808",
                phone: "9842259851"
            });
        expect(response.statusCode).toBe(200);
    });

    test('Verify if super admin able to update practice with same name', async () => {
        const response = await server.put(`/practices/${config.practice_id[2]}`)
            .set(config.headers)
            .set('Authorization', process.env.authToken)
            .send({
                name: "Super admin able to update practice",
                address: "updated Address",
                street: "updated West",
                city: "updated Madurai",
                state: "updated TamilNadu",
                zip_code: "627808",
                phone: "9842259851"
            });
        expect(response.statusCode).toBe(200);
    });

    test('Verify if super admin able to update practice without name field', async () => {
        const response = await server.put(`/practices/${config.practice_id[2]}`)
            .set(config.headers)
            .set('Authorization', process.env.authToken)
            .send({
                address: "updated Address",
                street: "updated West",
                city: "updated Madurai",
                state: "updated TamilNadu",
                zip_code: "627808",
                phone: "9842259851"
            });
        expect(response.statusCode).toBe(400);
    });

    //manager login
    test('manager login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[5],
                password: 'Test@123'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    test('Verify if manager able to update practice', async () => {
        const response = await server.put(`/practices/${config.practice_id[3]}`)
            .set(config.headers)
            .set('Authorization', process.env.authToken)
            .send({
                name: "Manager able to update practice",
                address: "updated Address",
                street: "updated West",
                city: "updated Madurai",
                state: "updated TamilNadu",
                zip_code: "627808",
                phone: "9842259851"
            });
        expect(response.statusCode).toBe(200);
    });

    test('Verify if manager not able to update practice name already exists', async () => {
        const response = await server.put(`/practices/${config.practice_id[2]}`)
            .set(config.headers)
            .set('Authorization', process.env.authToken)
            .send({
                name: "Manager able to update practice",
                address: "updated Address",
                street: "updated West",
                city: "updated Madurai",
                state: "updated TamilNadu",
                zip_code: "627808",
                phone: "9842259851"
            });
        expect(response.statusCode).toBe(404);
    });

    //operator login
    test('Operator login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[3],
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    test('Verify if operator able to update practice', async () => {
        const response = await server.put(`/practices/${config.practice_id[3]}`)
            .set(config.headers)
            .set('Authorization', process.env.authToken)
            .send({
                name: "Operator able to update practice",
                address: "updated Address",
                street: "updated West",
                city: "updated Madurai",
                state: "updated TamilNadu",
                zip_code: "627808",
                phone: "9842259851"
            });
        expect(response.statusCode).toBe(200);
    });

});