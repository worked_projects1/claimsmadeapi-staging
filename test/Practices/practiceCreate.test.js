const request = require('supertest');
const { config } = require('../global');
const server = request(config.server_url);

describe('Practice create validation', () => {

    //superAdmin login
    test('Super admin login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[0],
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    test('Verify if super admin able to create practice', async () => {
        const response = await server.post('/rest/practices')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({
                id: config.practice_id[2],
                name: "Super admin able to create practice",
                address: "Test Address",
                street: "West",
                city: "Madurai",
                state: "TamilNadu",
                zip_code: "627808",
                phone: "9842259851"
            });
        expect(response.statusCode).toBe(200);
    });

    test('Verify if super admin able to create practice with existing practice name', async () => {
        const existingPracticeName = "Super admin able to create practice";
        const response = await server.post('/rest/practices')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({
                id: config.practice_id[5],
                name: existingPracticeName,
                address: "Test Address",
                street: "West",
                city: "Madurai",
                state: "TamilNadu",
                zip_code: "627808",
                phone: "9842259851"
            });
        expect(response.statusCode).toBe(400);
        expect(JSON.parse(response.text).error).toBe(`Practice with name: ${existingPracticeName} already exist`)
    });

    test('Verify if super admin able to create practice with empty name', async () => {
        const response = await server.post('/rest/practices')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({
                id: config.practice_id[6],
                name: "",
                address: "Test Address",
                street: "West",
                city: "Madurai",
                state: "TamilNadu",
                zip_code: "627808",
                phone: "9842259851"
            });
        expect(response.statusCode).toBe(400);
        expect(JSON.parse(response.text).error.name[0]).toBe('The name field is required.')
    });

    //manager login
    test('Manager login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[2],
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    test('Verify if manager able to create practice', async () => {
        const response = await server.post('/rest/practices')
            .set(config.headers)
            .set('Authorization', process.env.authToken)
            .send({
                id: config.practice_id[3],
                name: "Manager able to create practice",
                address: "Test Address",
                street: "West",
                city: "Madurai",
                state: "TamilNadu",
                zip_code: "627808",
                phone: "9842259851"
            });
        expect(response.statusCode).toBe(200);
    });

    //operator login
    test('Opereator login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[3],
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    test('Verify if operator able to create practice', async () => {
        const response = await server.post('/rest/practices')
            .set(config.headers)
            .set('Authorization', process.env.authToken)
            .send({
                id: config.practice_id[4],
                name: "Operator able to create practice",
                address: "Test Address",
                street: "West",
                city: "Madurai",
                state: "TamilNadu",
                zip_code: "627808",
                phone: "9842259851"
            });
        expect(response.statusCode).toBe(200);
        // expect(JSON.parse(response.text).error).toBe('you don\'t have access');
    });

});