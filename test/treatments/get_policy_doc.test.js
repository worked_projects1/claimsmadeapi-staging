const request = require('supertest');
const { config } = require('../global');
const server = request(config.server_url);

describe('Practice create validation', () => {

    //superAdmin login
    test('Super admin login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[0],
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    test('Verify if the super admin is able to get the policyDoc', async () => {
        const response = await server.post('/getPolicyDoc')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({
                body_system: config.body_systems[0],
                treatment_id: config.treatmentTestRecordIds[0]
            });
        expect(response.statusCode).toBe(200);
    });

    //manager login
    test('Manager login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[2],
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    test('Verify if manager is able to get the policyDoc', async () => {
        const response = await server.post('/getPolicyDoc')
            .set(config.headers)
            .set('Authorization', process.env.authToken)
            .send({
                body_system: config.body_systems[0],
                treatment_id: config.treatmentTestRecordIds[0]
            });
        expect(response.statusCode).toBe(403);
        expect(JSON.parse(response.text).error).toBe('you don\'t have access');
    });

    //operator login
    test('Opereator login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[3],
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    test('Verify if operator is able to get the policyDoc', async () => {
        const response = await server.post('/getPolicyDoc')
            .set(config.headers)
            .set('Authorization', process.env.authToken)
            .send({
                body_system: config.body_systems[0],
                treatment_id: config.treatmentTestRecordIds[0]
            });
        expect(response.statusCode).toBe(403);
        expect(JSON.parse(response.text).error).toBe('you don\'t have access');
    });

});