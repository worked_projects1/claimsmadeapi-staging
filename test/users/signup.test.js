

const request = require('supertest');
const { config } = require('../global');

const server = request(config.server_url);

describe('SignUp', () => {

  test('Verify physician able to signup', async () => {
    const response = await server.post('/users/signup')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .send({
        practice_name: 'Test Practice1',
        name: 'Test user',
        email: config.testEmail,
        password: 'Test123$',
        role: 'physician',
        practice_id: config.practice_id[0],
        user_id: config.test_user_ids[0]
      });
    expect(response.statusCode).toBe(200);
    expect(JSON.parse(response.text).authToken).not.toBe('');
  });

  test('Verify staff able to signup', async () => {
    const response = await server.post('/users/signup')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .send({
        practice_name: 'Test Practice2',
        name: 'Test user2',
        email: config.testEmail2,
        password: 'Test123$',
        role: 'staff',
        practice_id: config.practice_id[7],
        user_id: config.test_user_ids[5]
      });
    expect(response.statusCode).toBe(200);
    expect(JSON.parse(response.text).authToken).not.toBe('');
  });

  test('Verify if the practice name is already exist', async () => {
    const response = await server.post('/users/signup')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .send({
        practice_name: 'Test Practice1',
        name: 'Test user',
        email: 'testpractice@gmail.com',
        password: 'Test123$',
        role: 'physician',
        practice_id: config.practice_id[0],
        user_id: config.test_user_ids[0]
      });
    expect(response.statusCode).toBe(400);
    expect(JSON.parse(response.text).error).toBe("Practice with name: Test Practice1 already exist");
  });

  test('Verify if email is already exist', async () => {
    const response = await server.post('/users/signup')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .send({
        practice_name: 'Test Practice2',
        name: 'Test user',
        email: config.testEmail,
        password: 'Test123$',
        role: 'physician',
        practice_id: config.practice_id[0],
        user_id: config.test_user_ids[0]
      });
    expect(response.statusCode).toBe(400);
    expect(JSON.parse(response.text).error).toBe("User with email: " + config.testEmail + " already exist");
  });

  test('Password validation for alphabetic character', async () => {
    const response = await server.post('/users/signup')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .send({
        practice_name: 'Test Practice2',
        name: 'Test user',
        email: config.testEmail,
        password: '123$',
        role: 'physician',
        practice_id: config.practice_id[0],
        user_id: config.test_user_ids[0]
      });
    expect(response.statusCode).toBe(400);
    expect(JSON.parse(response.text).error).toBe("Password must contain one alphabetic character");
  });

  test('Password validation for numeric or special character', async () => {
    const response = await server.post('/users/signup')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .send({
        practice_name: 'Test Practice',
        name: 'Test user',
        email: config.testEmail,
        password: 'Test',
        role: 'physician',
        practice_id: config.practice_id[0],
        user_id: config.test_user_ids[0]
      });
    expect(response.statusCode).toBe(400);
    expect(JSON.parse(response.text).error).toBe("Password must contain one numeric or special character");
  });

  test('Password validation for consecutive characters', async () => {
    const response = await server.post('/users/signup')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .send({
        practice_name: 'Test Practice',
        name: 'Test user',
        email: config.testEmail,
        password: 'TTTest1$',
        role: 'physician',
        practice_id: config.practice_id[0],
        user_id: config.test_user_ids[0]
      });
    expect(response.statusCode).toBe(400);
    expect(JSON.parse(response.text).error).toBe("Password must not have consecutive characters");
  });

});
