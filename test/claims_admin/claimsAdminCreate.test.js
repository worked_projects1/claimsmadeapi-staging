const request = require('supertest');
const { config } = require('../global');
const server = request(config.server_url);

describe('Claims Admin Create Validation', () => {

    //superAdmin login
    test('Super admin login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[0],
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    test('Verify if super admin can create Claims Admin user.', async () => {
        const response = await server.post('/claims_admin')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({
                id: 'claims_admin_id1',
                company_name: "Not ONe",
                email: config.claims_admin_email_ids[0],
                address: "Address check",
                city: "LA",
                state: "CA",
                zip_code: "52134",
                phone_number: "9876543218",
                fax_number: "56321456789"
            });
        console.log('Create CA response > ' + JSON.stringify(response))
        expect(response.statusCode).toBe(200);
    });

    test('Verify if super admin could not create Claims Admin (Same Company name).', async () => {
        const response = await server.post('/claims_admin')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({
                id: 'claims_admin_id1',
                email: config.claims_admin_email_ids[0],
                company_name: "Not ONe",
                address: "Address check",
                city: "LA",
                state: "CA",
                zip_code: "52134",
                phone_number: "9876543218",
                fax_number: "56321456789"
            });
        expect(response.statusCode).toBe(400);
        expect(JSON.parse(response.text).error).toBe(`Claims Administarator with Email: ${config.claims_admin_email_ids[0]} is already exists`)
    });

    test('Verify if super admin could not create Claims Admin (Same Company name).', async () => {
        const response = await server.post('/claims_admin')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({
                id: 'claims_admin_id1',
                email: config.claims_admin_email_ids[0],
                company_name: "Not ONe",
                address: "Address check",
                city: "LA",
                state: "CA",
                zip_code: "52134",
                phone_number: "9876543218",
                fax_number: "56321456789"
            });
        expect(response.statusCode).toBe(400);
        expect(JSON.parse(response.text).error).toBe(`Claims Administarator with Email: ${config.claims_admin_email_ids[0]} is already exists`)
    });

    test('Verify if super admin could not create Claims Admin (Same Company name).', async () => {
        const response = await server.post('/claims_admin')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({
                id: 'claims_admin_id3',
                email: config.claims_admin_email_ids[1],
                company_name: "Not ONe",
                address: "Address check",
                city: "LA",
                state: "CA",
                zip_code: "52134",
                phone_number: "9876543218",
                fax_number: "56321456789"
            });
        expect(response.statusCode).toBe(400);
        expect(JSON.parse(response.text).error).toBe(`Claims Administarator with Name: Not ONe is already exists`)
    });

    test('Verify if super admin able create Claims Admin with onlu mandatory fields.', async () => {
        const response = await server.post('/claims_admin')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({
                id: 'claims_admin_id3',
                email: config.claims_admin_email_ids[2],
                company_name: "Not ONe 01",
            });
        console.log('Response >> ' + JSON.stringify(response))
        expect(response.statusCode).toBe(200);
    });


    /*Need to write the test cases for Physician/staff logins*/

});