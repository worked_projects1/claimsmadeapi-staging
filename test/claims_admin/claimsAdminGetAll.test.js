const request = require('supertest');
const { config } = require('../global');
const server = request(config.server_url);

describe('Claims Admin get all validation', () => {

    //superAdmin login
    test('Super admin login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[0],
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    test('Verify if super admin able to get all the claims admins', async () => {
        const response = await server.get('/rest/claims_admin')
            .set(config.headers)
            .set({
                offset: 0,
                limit: 100,
                sort: false,
                search: false
            })
            .set('Authorization', process.env.authToken)
        console.log(JSON.stringify(response))
        expect(response.statusCode).toBe(200);
    });

    test('Verify if totalpagecount response header is exposing or not.', async () => {
        const response = await server.get('/rest/claims_admin')
            .set(config.headers)
            .set({
                offset: 0,
                limit: 100,
                sort: false,
                search: false
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
        expect(parseInt(response.header.totalpagecount)).toBeGreaterThan(0);
    });

});