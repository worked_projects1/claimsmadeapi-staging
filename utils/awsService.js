const AWS = require('aws-sdk');
const { HTTPError } = require('./httpResp');
AWS.config.update({ region: process.env.REGION || 'us-west-2' });
const s3 = new AWS.S3();
const fs = require('fs');
AWS.config.setPromisesDependency(require('bluebird'));
const shell = require('shelljs');

const getSignedUrlForS3 = async (event, s3FileKey, bucketName, content_type, accessParam, metaData) => {

    if (!metaData) {
        let userObj = {
            user_id: event.user.id
        }
        metaData = userObj;
    }

    const s3Params = {
        Bucket: bucketName, // S3 bucket name
        Key: s3FileKey, //Should be unique (eg: timeStamp)
        ContentType: content_type,  // application/pdf // application/json
        ACL: accessParam,  // 'private' or 'public-read'
        Metadata: metaData  //Optional
    };
    return new Promise((resolve, reject) => {
        const uploadURL = s3.getSignedUrl('putObject', s3Params);
        resolve({
            uploadURL,
            s3_file_key: s3Params.Key,
        });
    });
};

//To get the upload URL from s3

module.exports.getS3UploadURL = async (event, s3FileKey, bucketName, content_type, accessParam, metaData) => {
    try {
        const uploadURLObject = await getSignedUrlForS3(event, s3FileKey, bucketName, content_type, accessParam, metaData);
        return uploadURLObject;
    } catch (err) {
        console.log(err);
        throw new HTTPError(404, err.message || 'Could not get the Upload URL.');
    }
};

// To get the public URL with expire time
module.exports.getSecuredS3PublicUrl = async (bucketName, bucketRegion, s3_file_key) => {
    try {
        AWS.config.update({ region: bucketRegion });
        const publicURL = await s3.getSignedUrlPromise('getObject', {
            Bucket: bucketName,
            Expires: 60 * 60 * 1,
            Key: s3_file_key,
        });
        return publicURL;
    } catch (err) {
        console.log(err);
        throw new HTTPError(404, err.message || 'Could not get the public URL.');
    }
};

module.exports.getDownloadUrl = async (s3Bucket, fileKey, contentType, bucketRegion) => {
    try {
        AWS.config.update({ region: bucketRegion });    
        const publicUrl = await s3.getSignedUrlPromise('getObject', {
          Bucket: s3Bucket,
          Expires: 60 * 60 * 1,
          Key: fileKey,
          ResponseContentDisposition: 'attachment; filename ="' + fileKey + contentType + '"'
        });
        return {
          statusCode: 200,
          headers: {
            'Content-Type': 'text/plain',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true
          },
          body: {
            publicUrl: publicUrl
          }
        };
      } catch (err) {
        console.log(err);
        return {
          statusCode: err.statusCode || 500,
          headers: {
            'Content-Type': 'text/plain',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true
          },
          body: JSON.stringify({ error: err.message || 'Could not get the download URL.' }),
        };
      }
}

//To merge the multiple pdf files using their s3 file keys
module.exports.mergePdfFiles = async (s3FileKeys, read_bucket_name, upload_bucket_name, merged_file_key) => {

    try {
        let filesToMerge = "";
        //read_bucket_name - From which bucket we need to read the file content
        //upload_bucket_name - In which bucket we need to upload the merged file content


        //Download the files from S3 to the /tmp directory
        for (const filekey of s3FileKeys) {

            const paramsFile = {
                Bucket: read_bucket_name,
                Key: filekey
            };

            let readStream = await s3.getObject(paramsFile).promise();

            await fs.promises.writeFile(`/tmp/${filekey}`, readStream.Body);

            filesToMerge += `/tmp/${filekey}` + " ";
        }

        //Merge the files stored in /tmp
        await shell.exec(`gs -dNOPAUSE -sDEVICE=pdfwrite -sOUTPUTFILE=/tmp/result.pdf -dBATCH ${filesToMerge}`);

        //Upload the result to S3
        const fileContent = await fs.createReadStream(`/tmp/result.pdf`);

        const params = {
            Bucket: upload_bucket_name,
            Key: merged_file_key,
            Body: fileContent,
            ContentType: "application/pdf"
        };

        const uploadResponse = await s3.upload(params).promise();

        return uploadResponse;
    } catch (err) {
        console.log(err);
        throw new HTTPError(404, err.message || 'Could not merge the pdf files.');
    }
}
