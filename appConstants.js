module.exports.appConstants = {
    STATUS_ACTIVE: "Active",
    STATUS_INACTIVE: "Inactive",
    ADMIN_EMAIL_IDS: ['siva@miotiv.com'],
    USER_ROLES: ['physician', 'staff'],
    ROLE_PHYSICIAN: 'physician',
    ROLE_STAFF: 'staff',
    POLICY_TYPES: ['surgery', 'surgery-add-on', 'other'],
    APP_NAME: 'VettedClaims'
}