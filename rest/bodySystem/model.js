const { type } = require("os");

module.exports = (sequelize, type) => sequelize.define('BodySystem', {
    id: {
        type: type.STRING,
        primaryKey: true
    },
    name: type.STRING,
    index: type.STRING,
    created_by: type.STRING,
    is_deleted: type.BOOLEAN
}, {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
});