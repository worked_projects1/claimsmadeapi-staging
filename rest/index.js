const serverless = require('serverless-http');
const bodyParser = require('body-parser');
const express = require('express');
const cors = require('cors');

const healthCheckApi = require('../handler');
const authMiddleware = require('../authExpress');
const usersApi = require('./users/controller');
const practicesApi = require('./practices/controller');
const testApi = require('./Test_api_module/controller');
const treatmentApi = require('./treatments/controller');
const bodySystemApi = require('./bodySystem/controller');
const versionHistoryApi = require('./versionHistory/controller');
const rfaFormsApi = require('./rfa_forms/controller');
const claimsAdminApi = require('./claims_admin/controller');
const migrationApi = require('./migration/controller');
const app = express();
app.use(bodyParser.json({ strict: false }));

app.use(cors());
app.options('*', cors());

app.get('/rest/', authMiddleware, async (req, res) => {
  try {
    const responseData = await healthCheckApi.healthCheck(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/users', authMiddleware, async (req, res) => {
  try {
    const responseData = await usersApi.getAll(req);
    return res
      .status(responseData.statusCode).header(responseData.headers)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.post('/rest/users/session', async (req, res) => {
  try {
    const responseData = await usersApi.sessionTokenGenerate(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.delete('/rest/users/removeTestRecords', authMiddleware, async (req, res) => {
  try {
    const testApi = require('./Test_api_module/controller');
    const responseData = await testApi.destroyTestRecords(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.post('/rest/users/createTestSuperAdmin', authMiddleware, async (req, res) => {
  try {
    const responseData = await testApi.createTestSuperAdmin(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/practices', authMiddleware, async (req, res) => {
  try {
    const responseData = await practicesApi.getAll(req);
    return res
      .status(responseData.statusCode).header(responseData.headers)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/practicesNames', authMiddleware, async (req, res) => {
  try {
    const responseData = await practicesApi.getAllPracticesNames(req);
    return res
      .status(responseData.statusCode).header(responseData.headers)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/treatments', authMiddleware, async (req, res) => {
  try {
    const responseData = await treatmentApi.getAll(req);
    return res
      .status(responseData.statusCode).header(responseData.headers)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/diagnoses', authMiddleware, async (req, res) => {
  try {
    const responseData = await treatmentApi.getAllDiagnoses(req);
    return res
      .status(responseData.statusCode).header(responseData.headers)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/diagnosisNames', authMiddleware, async (req, res) => {
  try {
    const responseData = await treatmentApi.getAllDiagnosisNames(req);
    return res
      .status(responseData.statusCode).header(responseData.headers)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/bodySystem', authMiddleware, async (req, res) => {
  try {
    const responseData = await bodySystemApi.getAll(req);
    return res
      .status(responseData.statusCode).header(responseData.headers)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.put('/rest/users/reset-login-attempt', async (req, res) => {
  try {
    const responseData = await usersApi.resetLoginAttempt(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.post('/rest/getTreatmentDiagnosisByType', authMiddleware, async (req, res) => {
  try {
    const responseData = await treatmentApi.getTreatmentDiagnosisByType(req);
    return res
      .status(responseData.statusCode).header(responseData.headers)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/versionHistory', async (req, res) => {
  try {
    const responseData = await versionHistoryApi.getVersionHistory(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});
app.put('/rest/versionHistory', authMiddleware, async (req, res) => {
  try {
    const responseData = await versionHistoryApi.setVersionHistory(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/rfaForms', authMiddleware, async (req, res) => {
  try {
    const responseData = await rfaFormsApi.getAll(req);
    return res
      .status(responseData.statusCode).header(responseData.headers)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.put('/rest/users/reset-login-attempt', async (req, res) => {
  try {
    const responseData = await usersApi.resetLoginAttempt(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.put('/rest/user/update-signature', authMiddleware, async (req, res) => {
  try {
    const responseData = await usersApi.updateSignature(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.put('/rest/rfaForm/updateRfaFormDetails', authMiddleware, async (req, res) => {
  try {
    const responseData = await rfaFormsApi.updateRfaFormDetails(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/users/getAllUsersByRole', authMiddleware, async (req, res) => {
  try {
    const response = await usersApi.getAllUsersByRole(req);
    return res
      .status(response.statusCode).header(response.headers)
      .json(JSON.parse(response.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/claims_admin', authMiddleware, async (req, res) => {
  try {
    const response = await claimsAdminApi.getAll(req);
    return res
      .status(response.statusCode).header(response.headers)
      .json(JSON.parse(response.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/getAllBodySystemNames', authMiddleware, async (req, res) => {
  try {
    const responseData = await bodySystemApi.getAllBodySystemNames(req);
    return res
      .status(responseData.statusCode).header(responseData.headers)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.post('/rest/treatments/getAllTreatmentNames', authMiddleware, async (req, res) => {
  try {
    const responseData = await treatmentApi.getAllTreatmentNames(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.post('/rest/claims_admin', authMiddleware, async (req, res) => {
  try {
    const responseData = await claimsAdminApi.create(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.post('/rest/bodySystem', authMiddleware, async (req, res) => {
  try {
    const responseData = await bodySystemApi.create(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.post('/rest/users', authMiddleware, async (req, res) => {
  try {
    const responseData = await usersApi.create(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.post('/rest/practices', authMiddleware, async (req, res) => {
  try {
    const responseData = await practicesApi.create(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/getAllSyncData', async (req, res) => {
  try {
    const responseData = await treatmentApi.getAllSyncData(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.post('/rest/migration/importClaimsAdmins', async (req, res) => {
  try {
    const responseData = await migrationApi.importClaimsAdmins(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/updateAPIForsync', authMiddleware, async (req, res) => {
  try {
    const responseData = await treatmentApi.updateAPIForsync(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/claimsAdmin/getAllClaimsAdmins', authMiddleware, async (req, res) => {
  try {
    const responseData = await claimsAdminApi.getAllClaimsAdmins(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.post('/rest/migration/updateRfaFormDoc', authMiddleware, async (req, res) => {
  try {
    const responseData = await migrationApi.updateRfaFormDoc(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.post('/rest/treatments/getAllDiagnosisByBodysystemId', authMiddleware, async (req, res) => {
  try {
    const responseData = await treatmentApi.getAllDiagnosisByBodysystemId(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/treatments/getAllTreatmentDetails', authMiddleware, async (req, res) => {
  try {
    const responseData = await treatmentApi.getAllTreatmentDetails(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.post('/rest/treatments/getAllTreatmentByBodySystems', authMiddleware, async (req, res) => {
  try {
    const responseData = await treatmentApi.getAllTreatmentByBodySystems(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});


module.exports.handler = serverless(app);
