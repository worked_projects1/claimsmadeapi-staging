const { HTTPError } = require('../../utils/httpResp');
const { appConstants } = require('../../appConstants');
const claimsMadeRoles = ['physician', 'staff', 'superAdmin', 'manager', 'operator'];
const doesntHaveAccess = ["operator"];
const userReadAccessRoles = ['physician'];


exports.authorizeUpdate = function (user) {
    if (!claimsMadeRoles.includes(user.role) || user.is_admin === false || doesntHaveAccess.includes(user.role)) {
      throw new HTTPError(403, 'you don\'t have access');
    }
  };

exports.authorizeCreate = function (user) {
    if (!claimsMadeRoles.includes(user.role) || user.is_admin === false || doesntHaveAccess.includes(user.role)) {
      throw new HTTPError(403, 'you don\'t have access');
    }
  };

exports.authorizeGetAll = function (user) {
    if (!userReadAccessRoles.includes(user.role) || !user.is_admin) {
      throw new HTTPError(403, 'you don\'t have access');
    }
  };

exports.authorizeDelete = function (user) {
  if (!claimsMadeRoles.includes(user.role) || user.is_admin === false || doesntHaveAccess.includes(user.role)) {
    throw new HTTPError(403, 'you don\'t have access');
  }
}

exports.authorizeGetAllUsersByRole = function (role) {
  if (appConstants.ROLE_STAFF != role) {
      throw new HTTPError(403, 'you don\'t have access');
  }
};

exports.authorizeUpdateAuth = function (user) {
  if (appConstants.ROLE_PHYSICIAN != user.role) {
      throw new HTTPError(403, 'you couldn\'t update access');
  }
};