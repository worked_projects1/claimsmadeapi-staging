const Validator = require('validatorjs');
const { HTTPError } = require('../../utils/httpResp');

exports.validateCreatePractices = function (data) {
  const rules = {
    name: 'required|min:4|max:64',
    address: 'min:4|max:64',
    logoFile: 'min:4',
  };
  const validation = new Validator(data, rules);

  if (validation.fails()) {
    throw new HTTPError(400, validation.errors.all());
  }
};

exports.validateDeletePractice = function (data) {
  const rules = {
    id: 'required',
  };
  const validation = new Validator(data, rules);

  if (validation.fails()) {
    throw new HTTPError(400, validation.errors.all());
  }
};

exports.validateUpdatePractices = function (data) {
  const rules = {
    name: 'required|min:4|max:64',
    address: 'min:4|max:64',
    logoFile: 'min:4',
  };
  const validation = new Validator(data, rules);

  if (validation.fails()) {
    throw new HTTPError(400, validation.errors.all());
  }
};

exports.validateGetOnePractice = function (data) {
  const rules = {
    id: 'required',
  };
  const validation = new Validator(data, rules);

  if (validation.fails()) {
    throw new HTTPError(400, validation.errors.all());
  }
};
