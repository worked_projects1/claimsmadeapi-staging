const middy = require('@middy/core');
const doNotWaitForEmptyEventLoop = require('@middy/do-not-wait-for-empty-event-loop');
const authMiddleware = require('../../auth');
const uuid = require('uuid');
const { QueryTypes } = require('sequelize')
const connectToDatabase = require('../../db');
const { HTTPError } = require('../../utils/httpResp');
const { authorizeCreate, authorizeUpdate, authorizeGetOne, authorizeDestroy } = require('./authorization');
const { validateCreate, validateUpdateUser, validateGetOne, validateDestroy } = require('./validation');


const create = async (event) => {
    try {
        const input = typeof event.body == 'string' ? JSON.parse(event.body) : event.body;
        validateCreate(input);
        // authorizeCreate(event.user);
        const { ClaimsAdmins } = await connectToDatabase();

        const isAlreadyExist = await ClaimsAdmins.findOne({
            where: { email: input.email, is_claims_admin: true }
        });
        if (isAlreadyExist) throw new HTTPError(400, `Claims Administarator with Email: ${input.email} is already exists`);

        const isNameAlreadyExist = await ClaimsAdmins.findOne({
            where: { company_name: input.company_name, is_claims_admin: true }
        });
        if (isNameAlreadyExist) throw new HTTPError(400, `Claims Administarator with Name: ${input.company_name} is already exists`);

        const claimsAdminObj = Object.assign(input, {
            id: uuid.v4(),
            is_claims_admin: true,
            practice_id: null,
            created_by: event.user.id
        });
        const response = await ClaimsAdmins.create(claimsAdminObj);

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(response)
        }
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create the users.' }),
        };
    }
}

const update = async (event) => {
    try {
        const input = typeof event.body == 'string' ? JSON.parse(event.body) : event.body;
        const params = event.params || event.pathParameters;
        validateUpdateUser(input);
        authorizeUpdate(event.user);
        const { ClaimsAdmins, Op } = await connectToDatabase();

        const isNameAlreadyExists = await ClaimsAdmins.findOne({
            where: {
                id: { [Op.not]: params.id },
                company_name: input.company_name,
                is_claims_admin: true
            }
        });
        if (isNameAlreadyExists) throw new HTTPError(400, `Claims Administarator with Name: 
            ${input.company_name} already exist.`);

        const isEmailAlreadyExists = await ClaimsAdmins.findOne({
            where: {
                id: { [Op.not]: params.id },
                email: input.email,
                is_claims_admin: true
            }
        });
        if (isEmailAlreadyExists) throw new HTTPError(400, `Claims Administarator with Email: ${input.email} already exist.`);

        const claimsAdmin = await ClaimsAdmins.findOne({
            where: { id: params.id }
        });
        if (!claimsAdmin) throw new HTTPError(404, `No record found.`)

        input.is_claims_admin = true;
        input.practice_id = null;
        const updatedUser = Object.assign(claimsAdmin, input);
        const response = await updatedUser.save();

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(response)
        }
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not update the users.' }),
        };
    }
}

const getOne = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        validateGetOne(params);
        // authorizeGetOne(event.user);
        const { ClaimsAdmins } = await connectToDatabase();
        const response = await ClaimsAdmins.findOne({
            where: { id: params.id }
        });
        if (!response) throw new HTTPError(404, `No Claims Administarator User Found`);

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(response)
        }

    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the users.' }),
        };
    }
}

const destroy = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        validateDestroy(params);
        authorizeDestroy(event.user);
        const { ClaimsAdmins,sequelize } = await connectToDatabase();

        const user = await ClaimsAdmins.destroy({ where: { id: params.id } });
        if (user < 1) throw new HTTPError(404, 'User not found');

        //To clear the id from the existing RFA form records
        await sequelize.query(`UPDATE RfaForms SET claims_admin_id = null WHERE claims_admin_id = '${params.id}'`);

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: "Ok",
                message: "Deleted"
            })
        }
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the users.' }),
        };
    }
}

const getAll = async (event) => {
    try {
        const query = event.headers;
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        query.sort = typeof query.sort == "string" ? JSON.parse(query.sort) : query.sort
        const { sequelize } = await connectToDatabase();

        let selectQuery = `SELECT * FROM ClaimsAdmins WHERE ClaimsAdmins.is_claims_admin IS true `;
        let countQuery = `SELECT COUNT(*) FROM ClaimsAdmins WHERE ClaimsAdmins.is_claims_admin IS true `;

        let searchQuery = '';
        if (query.search != "false") {
            searchQuery = `AND ( ClaimsAdmins.company_name LIKE '%${query.search}%' OR 
        ClaimsAdmins.email LIKE '%${query.search}%' OR
        ClaimsAdmins.address LIKE '%${query.search}%' OR
        ClaimsAdmins.city LIKE '%${query.search}%' OR
        ClaimsAdmins.state LIKE '%${query.search}%' OR
        ClaimsAdmins.zip_code LIKE '%${query.search}%' OR
        ClaimsAdmins.phone_number LIKE '%${query.search}%' OR
        ClaimsAdmins.fax_number LIKE '%${query.search}%' )`
        }

        let orderQuery;
        if (query.sort && query.sort != "false") {
            orderQuery = ` ORDER BY ${query.sort.column} ${query.sort.type}`;
        } else {
            orderQuery = ` ORDER BY created_at DESC`;
        }

        let limitQuery;
        limitQuery = ` LIMIT ${query.limit} OFFSET ${query.offset};`;

        selectQuery = selectQuery + searchQuery + orderQuery + limitQuery;
        countQuery = countQuery + searchQuery;

        const data = await sequelize.query(selectQuery, {
            type: QueryTypes.SELECT
        });
        const tableDataCount = await sequelize.query(countQuery, {
            type: QueryTypes.SELECT
        });

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Expose-Headers': 'totalPageCount',
                'totalPageCount': tableDataCount[0]["COUNT(*)"]
            },
            body: JSON.stringify(data)
        }
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the users.' }),
        };
    }
}

const getClaimsUsersNames = async (event) => {
    try {
        const { sequelize } = await connectToDatabase();
        const sqlQuery = `SELECT ClaimsAdmins.id, ClaimsAdmins.company_name FROM ClaimsAdmins WHERE ClaimsAdmins.is_claims_admin IS true OR 
        ClaimsAdmins.practice_id = '${event.user.practice_id}'; `;

        const response = await sequelize.query(sqlQuery, {
            type: QueryTypes.SELECT
        })
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(response)
        }
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the users.' }),
        };
    }
}

const getAllClaimsAdmins = async (event) => {
    try {
        const { sequelize } = await connectToDatabase();
        let sqlQuery = `SELECT id, company_name, email, address, city, state, zip_code, contact_name FROM ClaimsAdmins WHERE is_claims_admin IS TRUE;`
        const serverData = await sequelize.query(sqlQuery, {
            type: QueryTypes.SELECT
        });
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(serverData)
        }
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the users.' }),
        };
    }
}

module.exports.create = create;
module.exports.update = middy(update).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getOne = middy(getOne).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.destroy = middy(destroy).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getClaimsUsersNames = middy(getClaimsUsersNames).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getAll = getAll;
module.exports.getAllClaimsAdmins = getAllClaimsAdmins;
