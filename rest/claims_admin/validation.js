const Validator = require('validatorjs');
const { HTTPError } = require('../../utils/httpResp');


exports.validateCreate = function (data) {
    const rules = {
        company_name: 'required|min:4|max:64',
        email: 'required|email'
    }
    const validation = new Validator(data, rules);
    if (validation.fails()) throw new HTTPError(400, validation.errors.all())
}

exports.validateUpdateUser = function (data) {
    const rules = {
        company_name: 'required|min:4|max:64',
        email: 'required|email'
    }
    const validation = new Validator(data, rules);
    if (validation.fails()) throw new HTTPError(400, validation.errors.all())
}

exports.validateGetOne = function (data) {
    const rules = {
        id: 'required'
    }
    const validation = new Validator(data, rules);
    if (validation.fails()) throw new HTTPError(400, validation.errors.all())
}

exports.validateDestroy = function (data) {
    const rules = {
        id: 'required'
    }
    const validation = new Validator(data, rules);
    if (validation.fails()) throw new HTTPError(400, validation.errors.all())
}