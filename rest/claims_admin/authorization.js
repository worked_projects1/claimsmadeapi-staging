const { HTTPError } = require("../../utils/httpResp")

const majorRoles = ['superAdmin', 'manager', 'opertor']

exports.authorizeCreate = function (user) {
    if (!majorRoles.includes(user.role)) throw new HTTPError(400, `You don\'t have access`)
}

exports.authorizeUpdate = function (user) {
    if (!majorRoles.includes(user.role)) throw new HTTPError(400, `You don\'t have access`)
}

exports.authorizeGetOne = function (user) {
    if (!majorRoles.includes(user.role)) throw new HTTPError(400, `You don\'t have access`)
}

exports.authorizeDestroy = function (user) {
    if (!majorRoles.includes(user.role)) throw new HTTPError(400, `You don\'t have access`)
}