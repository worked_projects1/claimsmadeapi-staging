const { HTTPError } = require("../../utils/httpResp")
const majorRoles = ['superAdmin', 'manager', 'operator'];

exports.authorizeSettings = function (user) {
    if (!majorRoles.includes(user.role)) throw new HTTPError(404, `You don\'t have access`)
}