module.exports = (sequelize, type) => sequelize.define('TreatmentHistory', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    body_system_id: type.STRING,
    treatment_id: type.STRING,
    name: type.STRING,
    s3_file_key: type.STRING,
    policy_doc: type.TEXT,
    updated_by: type.STRING
}, {
    updatedAt: 'updated_at',
    createdAt: 'created_at'
});