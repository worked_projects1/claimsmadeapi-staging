const { appConstants } = require('../../appConstants');
const { HTTPError } = require('../../utils/httpResp');
const majorRoles = ['superAdmin'];
const allRoles = ['superAdmin', 'physician', 'staff'];

exports.authorizeCreate = function (user) {
  if (!majorRoles.includes(user.role)) {
    throw new HTTPError(403, 'you don\'t have access');
  }
};

exports.authorizeAdmin = function (user) {
  if (!appConstants.ADMIN_EMAIL_IDS.includes(user.email)) {
    throw new HTTPError(403, 'you don\'t have access');
  }
};

exports.authorizeUpdate = function (user) {
  if (!majorRoles.includes(user.role)) {
    throw new HTTPError(403, 'you don\'t have access');
  }
};

exports.authorizeGetAll = function (user) {
  if (user.is_admin == false) {
    throw new HTTPError(403, 'you don\'t have access');
  }
};

exports.authorizeGetPolicies = function (user) {
  if (user.is_admin == false) {
    throw new HTTPError(403, 'you don\'t have access');
  }
};

exports.authorizeDestroy = function (user) {
  if (!majorRoles.includes(user.role)) {
    throw new HTTPError(403, 'you don\'t have access');
  }
};

exports.authorizeGetOne = function (user) {
  if (user.is_admin == false) {
    throw new HTTPError(403, 'you don\'t have access');
  }
};