module.exports = (sequelize, type) => sequelize.define('Diagnosis', {
  id: {
    type: type.STRING,
    primaryKey: true
  },
  name: type.STRING,
  body_system_id: type.STRING,
  treatment_id: type.STRING,
  source: type.STRING,
  created_by: type.STRING
}, {
  freezeTableName: true,
  updatedAt: 'updated_at',
  createdAt: 'created_at'
});
